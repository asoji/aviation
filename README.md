# Aviation

> Aviation is a lightweight yet powerful command handler and arg parser for JDA, in Kotlin. It offers numerous utilities and functions for assisting, and speeding up Discord Bot development, allowing you to spend less time on the boring bits, and more on the important bits.

## Quick Start

You probably shouldn't use this, Aviation is a modified version of [Flight](https://github.com/Devoxin/Flight) used internally by Milo.

We recommend using that instead

## Usage

Congratulations, we don't have documentation, but we stuck quite closely to [Flight's](https://github.com/Devoxin/Flight) code so it shouldn't be hard to figure out between the two. Also probably set your JDK version to 21 due to that being the minimum language level set for this library.