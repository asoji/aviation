package xyz.artrinix.aviation.command.message

import xyz.artrinix.aviation.annotations.RateLimit
import xyz.artrinix.aviation.command.message.annotations.MessageCommand
import xyz.artrinix.aviation.entities.Scaffold
import xyz.artrinix.aviation.internal.arguments.CommandArgument
import xyz.artrinix.aviation.internal.entities.ICommand
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter

data class MessageCommandFunction(
    override val name: String,
    override val method: KFunction<*>,
    override val scaffold: Scaffold,
    override val arguments: List<CommandArgument>,
    override val contextParameter: KParameter,
    override val category: String,
    val properties: MessageCommand,
    override val rateLimit: RateLimit?,
    val subCommands: List<MessageSubCommandFunction>
) : ICommand.Message, ICommand.Categorized, ICommand.HasRateLimit {
    val subcommands = hashMapOf<String, MessageSubCommandFunction>()

    init {
        for (sc in subCommands) {
            val triggers = listOf(sc.name, *sc.properties.aliases)

            for (trigger in triggers) {
                if (subcommands.containsKey(trigger)) {
                    throw IllegalStateException("The sub-command trigger $trigger already exists!")
                }

                subcommands[trigger] = sc
            }
        }
    }
}
