package xyz.artrinix.aviation.command.slash

import net.dv8tion.jda.api.interactions.commands.Command
import xyz.artrinix.aviation.command.slash.annotations.SlashCommand
import xyz.artrinix.aviation.entities.Scaffold
import xyz.artrinix.aviation.internal.arguments.CommandArgument
import xyz.artrinix.aviation.internal.entities.ICommand
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter

data class SlashCommandFunction(
    override val name: String,
    override val method: KFunction<*>?,
    override val scaffold: Scaffold,
    override val arguments: List<CommandArgument>,
    override val contextParameter: KParameter?,
    override val category: String,
    val properties: SlashCommand,
    val subCommands: List<SlashSubCommandFunction>,
    val subCommandGroups: Map<String, SubCommandGroup>
) : ICommand.Slash, ICommand.Categorized {
    val subCommandMap: Map<String, SlashSubCommandFunction>
        get() = subCommands.associateBy { it.name }

    /**
     * The command reference.
     */
    var ref: Command? = null
        internal set
}