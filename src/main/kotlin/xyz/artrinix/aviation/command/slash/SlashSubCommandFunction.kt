package xyz.artrinix.aviation.command.slash

import xyz.artrinix.aviation.command.slash.annotations.SlashSubCommand
import xyz.artrinix.aviation.entities.Scaffold
import xyz.artrinix.aviation.internal.arguments.CommandArgument
import xyz.artrinix.aviation.internal.entities.ICommand
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter

data class SlashSubCommandFunction(
    override val name: String,
    override val method: KFunction<*>,
    override val scaffold: Scaffold,
    override val arguments: List<CommandArgument>,
    override val contextParameter: KParameter,
    override val declaringClass: Any = scaffold,
    val properties: SlashSubCommand,
) : ICommand.Slash