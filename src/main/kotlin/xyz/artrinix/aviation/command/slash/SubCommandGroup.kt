package xyz.artrinix.aviation.command.slash

/**
 * Represents a sub-command group.
 */
data class SubCommandGroup(
    val name: String,
    val description: String = "No description provided",
    val commands: List<SlashSubCommandFunction>
) {
    val commandMap: Map<String, SlashSubCommandFunction>
        get() = commands.associateBy { it.name }
}
