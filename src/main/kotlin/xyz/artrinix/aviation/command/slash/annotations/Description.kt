package xyz.artrinix.aviation.command.slash.annotations

annotation class Description(val value: String)
