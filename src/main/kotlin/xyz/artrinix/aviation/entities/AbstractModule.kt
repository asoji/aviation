package xyz.artrinix.aviation.entities

interface AbstractModule {
    suspend fun onEnable()
    suspend fun onDisable()
}