package xyz.artrinix.aviation.entities

import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.Invite

data class Invite(
    private val jda: JDA,
    val url: String,
    val code: String
) {
    fun resolve() = Invite.resolve(jda, code)
}