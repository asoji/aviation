package xyz.artrinix.aviation.events

/**
 * Emitted when an internal error occurs within Aviation.
 */
data class AviationExceptionEvent(val error: Throwable) : Event
