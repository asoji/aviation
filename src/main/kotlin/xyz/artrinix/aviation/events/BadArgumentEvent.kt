package xyz.artrinix.aviation.events

import xyz.artrinix.aviation.command.Context
import xyz.artrinix.aviation.exceptions.BadArgument
import xyz.artrinix.aviation.internal.entities.ICommand

/**
 * Emitted when an invalid argument is passed.
 */
data class BadArgumentEvent(
    val ctx: Context,
    val command: ICommand,
    val error: BadArgument
) : Event