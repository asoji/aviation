package xyz.artrinix.aviation.events

import xyz.artrinix.aviation.command.Context
import xyz.artrinix.aviation.internal.entities.ICommand

/**
 * Emitted when an [ICommand] is executed in a bad environment.
 *
 * @param ctx
 *   The [Context] instance.
 *
 * @param command
 *   The [ICommand] that was executed.
 *
 * @param reason
 *   The reason it was a bad environment.
 */
data class BadEnvironmentEvent(
    val ctx: Context,
    val command: ICommand,
    val reason: Reason
) : Event {
    enum class Reason {
        NonNSFW,
        NonGuild,
        NonDeveloper,
    }
}
