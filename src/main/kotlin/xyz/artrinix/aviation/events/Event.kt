package xyz.artrinix.aviation.events

/**
 * An event emitted by [Aviation]
 */
interface Event