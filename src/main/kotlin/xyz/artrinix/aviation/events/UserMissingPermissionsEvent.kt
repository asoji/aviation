package xyz.artrinix.aviation.events

import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.User
import xyz.artrinix.aviation.command.Context
import xyz.artrinix.aviation.internal.entities.ICommand

/**
 * Emitted when a user lacks [permissions] to execute [command]
 *
 * @param ctx
 *   The current command context.
 *
 * @param command
 *   The command
 *
 * @param permissions
 *   List of [Permission]s the user lacks.
 */
data class UserMissingPermissionsEvent(val ctx: Context, val command: ICommand, val permissions: List<Permission>) :
    Event {
    /**
     * The user that is lacking [permissions].
     */
    val user: User
        get() = ctx.author
}
