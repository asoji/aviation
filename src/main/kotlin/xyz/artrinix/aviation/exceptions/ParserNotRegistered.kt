package xyz.artrinix.aviation.exceptions

class ParserNotRegistered(msg: String) : Throwable(msg)