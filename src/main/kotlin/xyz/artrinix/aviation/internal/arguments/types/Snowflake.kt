package xyz.artrinix.aviation.internal.arguments.types

data class Snowflake(val resolved: Long)
