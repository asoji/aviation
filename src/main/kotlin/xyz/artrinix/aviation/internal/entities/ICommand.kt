package xyz.artrinix.aviation.internal.entities

import xyz.artrinix.aviation.annotations.RateLimit
import xyz.artrinix.aviation.command.Context
import xyz.artrinix.aviation.command.message.MessageCommandFunction
import xyz.artrinix.aviation.command.message.MessageSubCommandFunction
import xyz.artrinix.aviation.command.slash.SlashCommandFunction
import xyz.artrinix.aviation.command.slash.SlashSubCommandFunction
import xyz.artrinix.aviation.entities.Scaffold
import xyz.artrinix.aviation.internal.arguments.CommandArgument
import javax.naming.OperationNotSupportedException
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.full.callSuspendBy
import kotlin.reflect.full.instanceParameter

interface ICommand {
    val name: String
    val method: KFunction<*>?
    val scaffold: Scaffold
    val arguments: List<CommandArgument>
    val contextParameter: KParameter?
    val declaringClass: Any? get() = scaffold

    interface Slash : ICommand

    interface Message : ICommand

    interface HasRateLimit : ICommand {
        val rateLimit: RateLimit?
    }

    interface Categorized : ICommand {
        val category: String
    }

    val botPermissions
        get() = when(this) {
            is SlashSubCommandFunction -> properties.botPermissions
            is SlashCommandFunction -> properties.botPermissions
            is MessageSubCommandFunction -> properties.botPermissions
            is MessageCommandFunction -> properties.botPermissions
            else -> emptyArray()
        }

    val userPermissions
        get() = when(this) {
            is SlashCommandFunction -> properties.defaultUserPermissions
            is MessageSubCommandFunction -> properties.userPermissions
            is MessageCommandFunction -> properties.userPermissions
            else -> emptyArray()
        }

    suspend fun execute(ctx: Context, args: MutableMap<KParameter, Any?>): Result<Boolean> {
        method ?: throw OperationNotSupportedException()
        method!!.instanceParameter?.let { args[it] = declaringClass }

        args[contextParameter ?: error("Missing contextParameter field.")] = ctx

        return try {
            if (method!!.isSuspend) {
                method!!.callSuspendBy(args)
            } else {
                method!!.callBy(args)
            }

            Result.success(true)
        } catch (ex: Throwable) {
            Result.failure(ex)
        }
    }
}