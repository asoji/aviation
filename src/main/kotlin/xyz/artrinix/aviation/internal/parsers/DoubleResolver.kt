package xyz.artrinix.aviation.internal.parsers

import net.dv8tion.jda.api.interactions.commands.Command
import net.dv8tion.jda.api.interactions.commands.OptionMapping
import net.dv8tion.jda.api.interactions.commands.OptionType
import xyz.artrinix.aviation.command.message.MessageContext
import xyz.artrinix.aviation.command.slash.SlashContext
import java.util.*

class DoubleResolver : Resolver<Double> {
    override val optionType: OptionType = OptionType.NUMBER

    override suspend fun getOptionChoice(name: String, value: Any): Command.Choice {
        require(value is Double) { "$value is not a double." }
        return Command.Choice(name, value.toDouble())
    }

    override suspend fun resolveOption(ctx: SlashContext, option: OptionMapping): Optional<Double> =
        Optional.of(option.asDouble)

    override suspend fun resolve(ctx: MessageContext, param: String): Optional<Double> {
        return Optional.ofNullable(param.toDoubleOrNull())
    }
}
