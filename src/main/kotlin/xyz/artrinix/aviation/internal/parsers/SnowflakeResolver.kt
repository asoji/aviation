package xyz.artrinix.aviation.internal.parsers

import net.dv8tion.jda.api.interactions.commands.OptionMapping
import net.dv8tion.jda.api.interactions.commands.OptionType
import xyz.artrinix.aviation.command.message.MessageContext
import xyz.artrinix.aviation.command.slash.SlashContext
import xyz.artrinix.aviation.internal.arguments.types.Snowflake
import java.util.*
import java.util.regex.Pattern

object SnowflakeResolver : Resolver<Snowflake> {
    private val snowflakeMatch = Pattern.compile("^(?:<(?:@!?|@&|#)(?<sid>[0-9]{17,21})>|(?<id>[0-9]{17,21}))$")

    override val optionType: OptionType = OptionType.STRING

    override suspend fun resolveOption(ctx: SlashContext, option: OptionMapping): Optional<Snowflake> =
        parse(option.asString)

    override suspend fun resolve(ctx: MessageContext, param: String): Optional<Snowflake> =
        parse(param)

    private fun parse(param: String): Optional<Snowflake> {
        val match = snowflakeMatch.matcher(param)

        if (match.matches()) {
            val id = match.group("sid") ?: match.group("id")
            return Optional.of(Snowflake(id.toLong()))
        }

        return Optional.empty()
    }
}