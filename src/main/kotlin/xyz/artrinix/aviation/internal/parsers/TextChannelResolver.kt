package xyz.artrinix.aviation.internal.parsers

import net.dv8tion.jda.api.entities.channel.concrete.TextChannel
import net.dv8tion.jda.api.interactions.commands.OptionMapping
import net.dv8tion.jda.api.interactions.commands.OptionType
import xyz.artrinix.aviation.command.message.MessageContext
import xyz.artrinix.aviation.command.slash.SlashContext
import java.util.*

class TextChannelResolver : Resolver<TextChannel> {
    override val optionType: OptionType = OptionType.CHANNEL

    override suspend fun resolveOption(ctx: SlashContext, option: OptionMapping): Optional<TextChannel> =
        Optional.ofNullable(option.asChannel.asGuildMessageChannel() as? TextChannel)

    override suspend fun resolve(ctx: MessageContext, param: String): Optional<TextChannel> {
        val snowflake = SnowflakeResolver.resolve(ctx, param)
        val channel: TextChannel? = if (snowflake.isPresent) {
            ctx.guild?.getTextChannelById(snowflake.get().resolved)
        } else {
            ctx.guild?.textChannels?.firstOrNull { it.name == param }
        }

        return Optional.ofNullable(channel)
    }
}