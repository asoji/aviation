package xyz.artrinix.aviation.internal.utils

import org.reflections.Reflections
import org.reflections.scanners.MethodParameterNamesScanner
import org.reflections.scanners.Scanners
import org.slf4j.LoggerFactory
import xyz.artrinix.aviation.command.message.annotations.MessageCommand
import xyz.artrinix.aviation.entities.AbstractModule
import xyz.artrinix.aviation.entities.Scaffold
import java.lang.reflect.Modifier
import kotlin.reflect.KFunction
import kotlin.reflect.full.hasAnnotation

class Indexer(private val packageName: String) {
    companion object {
        private val logger = LoggerFactory.getLogger(Indexer::class.java)
    }

    private val reflections: Reflections = Reflections(packageName, MethodParameterNamesScanner(), Scanners.SubTypes)

    fun getModules(): List<AbstractModule> {
        val modules = reflections.getSubTypesOf(AbstractModule::class.java)
        logger.debug("Discovered ${modules.size} modules in $packageName")

        return modules
            .filter { !Modifier.isAbstract(it.modifiers) && !it.isInterface && AbstractModule::class.java.isAssignableFrom(it) }
            .map { it.getDeclaredConstructor().newInstance() }
    }

    fun getScaffolds(): List<Scaffold> {
        val scaffolds = reflections.getSubTypesOf(Scaffold::class.java)
        logger.debug("Discovered ${scaffolds.size} scaffolds in $packageName")

        return scaffolds
            .filter { !Modifier.isAbstract(it.modifiers) && !it.isInterface && Scaffold::class.java.isAssignableFrom(it) }
            .map { it.getDeclaredConstructor().newInstance() }
    }

    fun getMessageCommands(scaffold: Scaffold): List<KFunction<*>> {
        logger.debug("Scanning ${scaffold::class.simpleName} for message commands...")

        val scaffoldClass = scaffold::class
        val commands = scaffoldClass.members
            .filterIsInstance<KFunction<*>>()
            .filter { it.hasAnnotation<MessageCommand>() }

        logger.debug("Found ${commands.size} commands in scaffold ${scaffold::class.simpleName}")
        return commands.toList()
    }
}
