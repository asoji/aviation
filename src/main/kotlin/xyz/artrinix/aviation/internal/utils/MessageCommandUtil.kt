package xyz.artrinix.aviation.internal.utils

import org.slf4j.LoggerFactory
import xyz.artrinix.aviation.annotations.Name
import xyz.artrinix.aviation.annotations.RateLimit
import xyz.artrinix.aviation.command.Context
import xyz.artrinix.aviation.command.message.MessageCommandFunction
import xyz.artrinix.aviation.command.message.MessageSubCommandFunction
import xyz.artrinix.aviation.command.message.annotations.*
import xyz.artrinix.aviation.entities.Scaffold
import xyz.artrinix.aviation.internal.arguments.CommandArgument
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.full.*
import kotlin.reflect.jvm.javaMethod
import kotlin.reflect.jvm.jvmErasure
import kotlin.reflect.typeOf

object MessageCommandUtil {
    private val logger = LoggerFactory.getLogger(MessageCommandUtil::class.java)

    fun loadCommand(meth: KFunction<*>, scaffold: Scaffold): MessageCommandFunction {
        val name = meth.findAnnotation<Name>()?.name
            ?: meth.name

        check(meth.javaMethod!!.declaringClass == scaffold::class.java) {
            "$name is not from ${scaffold::class.simpleName}"
        }

        require(meth.hasAnnotation<MessageCommand>()) {
            "$name is not annotated with Command!"
        }

        val category = scaffold.name()
            ?: scaffold::class.java.`package`.name.split('.')
                .last()
                .replace('_', ' ')
                .lowercase()
                .replaceFirstChar { it.uppercase() }

        val properties = meth.findAnnotation<MessageCommand>()!!
        val rateLimit = meth.findAnnotation<RateLimit>()
        val ctxParam = meth.valueParameters.firstOrNull {
            it.type.isSubtypeOf(typeOf<Context>())
        }

        require(ctxParam != null) {
            "${meth.name} is missing the Context parameter!"
        }

        val parameters = meth.valueParameters.filterNot {
            it.type.isSubtypeOf(typeOf<Context>())
        }

        val arguments = loadParameters(parameters)
        val subcommands = getSubCommands(scaffold)
        val scaffoldParentCommands = scaffold::class.functions.filter {
            it.hasAnnotation<MessageCommand>()
        }

        if (subcommands.isNotEmpty() && scaffoldParentCommands.size > 1) {
            throw IllegalStateException("SubCommands are present within ${scaffold::class.simpleName} however there are multiple top-level commands!")
        }

        return MessageCommandFunction(
            name,
            meth,
            scaffold,
            arguments,
            ctxParam,
            category,
            properties,
            rateLimit,
            subcommands
        )
    }

    fun getSubCommands(scaffold: Scaffold): List<MessageSubCommandFunction> {
        logger.debug("Scanning ${scaffold::class.simpleName} for sub-commands...")

        val scaffoldClass = scaffold::class
        val subcommands = scaffoldClass.members
            .filterIsInstance<KFunction<*>>()
            .filter { it.hasAnnotation<MessageSubCommand>() }
            .map { loadSubCommand(it, scaffold) }

        logger.debug("Found ${subcommands.size} sub-commands in scaffold ${scaffoldClass.simpleName}")
        return subcommands.toList()
    }

    private fun loadSubCommand(meth: KFunction<*>, scaffold: Scaffold): MessageSubCommandFunction {
        /* get the name of this sub-command */
        val name = meth.findAnnotation<Name>()?.name
            ?: meth.name.lowercase()

        /* do some checks */
        check(meth.javaMethod!!.declaringClass == scaffold::class.java) {
            "${meth.name} is not from ${scaffold::class.simpleName}"
        }

        require(meth.hasAnnotation<MessageSubCommand>()) {
            "${meth.name} is not annotated with MessageSubCommand!"
        }

        /* load properties */
        val properties = meth.findAnnotation<MessageSubCommand>()!!

        /* find the message context parameter. */
        val ctxParam = meth.valueParameters.firstOrNull {
            it.type.isSubtypeOf(typeOf<Context>())
        }

        require(ctxParam != null) {
            "${meth.name} is missing the MessageContext parameter!"
        }

        /* find arguments */
        val parameters = meth.valueParameters.filterNot {
            it.type.isSubtypeOf(typeOf<Context>())
        }

        val arguments = loadParameters(parameters)

        /* return sub command function */
        return MessageSubCommandFunction(name, properties, meth, scaffold, arguments, ctxParam)
    }

    private fun loadParameters(parameters: List<KParameter>): List<CommandArgument> {
        val arguments = mutableListOf<CommandArgument>()

        for (p in parameters) {
            val pName = p.findAnnotation<Name>()?.name ?: p.name ?: p.index.toString()
            var type = p.type.jvmErasure.javaObjectType
            var greedy = p.findAnnotation<Greedy>()?.let { GreedyInfo(it.type, it.min..it.max) }
            val isOptional = p.isOptional
            val isNullable = p.type.isMarkedNullable
            val isTentative = p.hasAnnotation<Tentative>()

            if (p.type.jvmErasure.isSubclassOf(Collection::class)) {
                type = p.type.arguments.first().type!!.jvmErasure.javaObjectType
                greedy = GreedyInfo(GreedyType.Regular, greedy?.range ?: 1..Int.MAX_VALUE)
            } else if (greedy != null) {
                greedy = GreedyInfo(GreedyType.Computed, greedy.range)
            }

            if (isTentative && !(isNullable || isOptional)) {
                throw IllegalStateException("${p.name} is marked as tentative, but does not have a default value and is not marked nullable!")
            }

            arguments.add(CommandArgument(pName, type, greedy, isOptional, isNullable, isTentative, null, p))
        }

        return arguments
    }
}
