package xyz.artrinix.aviation.internal.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import xyz.artrinix.aviation.Aviation
import xyz.artrinix.aviation.AviationBuilder
import xyz.artrinix.aviation.events.Event
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

/**
 * Creates a new [Aviation] instance using the provided [builder]
 *
 * @param builder
 *   Used to create a new [Aviation] instance.
 */
@OptIn(ExperimentalContracts::class)
fun Aviation(builder: AviationBuilder.() -> Unit): Aviation {
    contract {
        callsInPlace(builder, InvocationKind.EXACTLY_ONCE)
    }

    return AviationBuilder().apply(builder).build()
}

@PublishedApi
internal val logger = LoggerFactory.getLogger("Aviation.on")

/**
 * Convenience method that will invoke the [consumer] whenever [T] is emitted on [Aviation.events]
 *
 * @param scope
 *   The scope to launch the consumer job in.
 *
 * @param consumer
 *   Event consumer for [T]
 *
 * @return A [Job] that can be used to cancel any further processing of [T]
 */
inline fun <reified T : Event> Aviation.on(
    scope: CoroutineScope = this,
    crossinline consumer: suspend T.() -> Unit
): Job {
    return events.buffer(Channel.UNLIMITED).filterIsInstance<T>()
        .onEach { event ->
            launch {
                event
                    .runCatching { event.consumer() }
                    .onFailure { err -> logger.error("Error while handling event ${T::class.simpleName}", err) }
            }
        }
        .launchIn(scope)
}